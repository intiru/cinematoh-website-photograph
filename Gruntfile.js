module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/jquery.min.js',
        'assets/template_front/js/jquery-migrate-3.0.0.min.js',
        'assets/template_front/js/jquery-2.1.4.min.js',
        'assets/template_front/js/modernizr.min.js',
        'assets/template_front/js/jquery.masonry.js',
        'assets/template_front/js/jquery.fancybox.pack.js',
        'assets/template_front/js/jquery.fancybox-thumbs.js?v=1.0.7',
        'assets/template_front/js/circliful.min.js',
        'assets/template_front/js/bootstrap.min.js',
        'assets/template_front/js/isotope.js',
        'assets/template_front/js/slick.min.js',
        'assets/template_front/js/tilt.min.js',
        'assets/template_front/js/wow.min.js',
        'assets/template_front/js/waypoints.min.js',
        'assets/template_front/js/counterup.min.js',
        'assets/template_front/js/jquery.magnific-popup.min.js',
        'assets/template_front/js/main.js'
    ];
    var cssFiles = [
        'assets/template_front/css/bootstrap.min.css',
        'assets/template_front/css/slick.css',
        'assets/template_front/css/slick-theme.css',
        'assets/template_front/css/flaticon.css',
        'assets/template_front/css/fontawesome-all.min.css',
        'assets/template_front/css/font-awesome.min.css',
        'assets/template_front/css/animate.min.css',
        'assets/template_front/css/magnific-popup.css',
        'assets/template_front/css/colors/gradient/color.css',
        'assets/template_front/css/colors/solid/color.css',
        'assets/template_front/css/colors/solid/color-2.css',
        'assets/template_front/css/price.css',
        'assets/template_front/css/style.css',
        'assets/template_front/css/jquery.fancybox.css',
        'assets/template_front/css/jquery.fancybox-thumbs.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/template_front/js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};