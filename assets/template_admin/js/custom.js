$(document).ready(function () {
    var base_url = $('#base_url').attr('title');
    var path_img = base_url + 'upload/images/';
    var input = '[name="id_category"], [name="id_parent"], [name="thumbnail"], [name="thumbnail_alt"], [name="description"], [name="meta_title"], [name="meta_description"], [name="meta_keywords"]';

    $('[name="data_type"]').change(function () {
        var value = $(this).val();

        $('[name="id_parent_2"]').val('0');

        if (value == 'top_menu') {
            $(input).parents('.form-group').hide();
            $('[name="id_parent_2"]').parents('.form-group').hide();
        } else if (value == 'sub_menu_1') {
            $(input).parents('.form-group').hide();
            $('[name="id_parent"]').parents('.form-group').show();
            $('[name="id_parent_2"]').parents('.form-group').hide();
        } else {
            $(input).parents('.form-group').show();
            $('[name="id_parent_2"]').parents('.form-group').show();
        }
    });

    $('[name="id_parent"]').change(function () {
        var id_parent = $(this).val();

        $.ajax({
            url: base_url+"intiru/product/sub_menu/" + id_parent,
            type: "GET",
            success: function (json) {
                var data = JSON.parse(json);
                var option = '<option value="0">Produk pada Top Menu</option>';
                $.each(data, function (key, val) {
                    console.log(key, val);
                    option += '<option value="' + val.id + '">' + val.title + '</option>';
                });

                $('[name="id_parent_2"]').html(option);

            }
        });
    });

    $('.datatable').on("click", ".btn-edit-produk", function (e) {
        e.preventDefault();
        var json = $(this).parents('td').siblings('td.data-row').children('textarea').val();
        var action = $(this).data('action');
        var data = JSON.parse(json);
        var id_parent_2 = data.id_parent_2;

        console.log(data);

        $('#modal-edit').parents('form').attr('action', action);
        $.each(data, function (field, value) {

            if ($('#modal-edit [name="' + field + '"]').hasClass('tinymce')) {
                tinyMCE.activeEditor.setContent(value);
            } else if ($('#modal-edit [name="' + field + '"]').hasClass('browse-preview-img')) {
                $('#modal-edit [name="' + field + '"]').parents('div').siblings('img').attr('src', path_img + value);
            } else {
                $('#modal-edit [name="' + field + '"]').val(value);
            }

        });

        if(data.data_type === 'top_menu') {
            $(input).parents('.form-group').hide();
            $('[name="id_parent_2"]').parents('.form-group').hide();
        } else if(data.data_type === 'sub_menu_1') {
            $(input).parents('.form-group').hide();
            $('[name="id_parent"]').parents('.form-group').show();
            $('[name="id_parent_2"]').parents('.form-group').hide();
        } else {
            $(input).parents('.form-group').show();
            $('[name="id_parent_2"]').parents('.form-group').show();
            var id_parent = $('#modal-edit [name="id_parent"]').val();

            $.ajax({
                url: base_url+"intiru/product/sub_menu/" + id_parent,
                type: "GET",
                success: function (json) {
                    var data = JSON.parse(json);
                    var option = '<option value="0">Produk pada Top Menu</option>';
                    $.each(data, function (key, val) {
                        console.log(key, val);
                        option += '<option value="' + val.id + '">' + val.title + '</option>';
                    });

                    $('#modal-edit [name="id_parent_2"]').html(option);

                    $('#modal-edit [name="id_parent_2"]').val(id_parent_2);

                }
            });
        }


        $('#modal-edit').modal('show');


        return false;
    });

});