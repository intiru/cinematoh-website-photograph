<?php echo $tab_language ?>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon2-image-file"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Management Tour
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="<?php echo base_url('intiru/tour') ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-backward"></i>
                        Back
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form method="post" action="<?php echo base_url() . 'intiru/tour/update/'.$edit->id; ?>" enctype="multipart/form-data"
              class="form-send">
            <input type="hidden" name="updated_at" value="<?php echo date('Y-m-d H:i:s') ?>">
            <div class="form-group">
                <label for="exampleSelect1">Category</label>
                <select class="form-control" name="id_category">
                    <?php foreach ($category as $r) { ?>
                        <option value="<?php echo $r->id ?>" <?php echo $r->id == $edit->id_category ? 'selected':'' ?> ><?php echo $r->title ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Use</label>
                <select class="form-control" name="use">
                    <option value="yes" <?php echo $edit->use == 'yes' ? 'selected':'' ?>>Yes</option>
                    <option value="no" <?php echo $edit->use == 'no' ? 'selected':'' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Tour Title</label>
                <input type="text" class="form-control" name="title" value="<?php echo $edit->title ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Tour Sub Title</label>
                <input type="text" class="form-control" name="title_sub" value="<?php echo $edit->title_sub ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Destination List</label>
                <div class="kt-checkbox-list">
                    <?php foreach($destination as $row) { ?>
                        <label class="kt-checkbox">
                            <input type="checkbox" name="id_destination_json[]" value="<?php echo $row->id ?>" <?php echo in_array($row->id, $edit->id_destination_json) ?'checked':'' ?>> <?php echo $row->title ?>
                            <span></span>
                        </label>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Thumbnail</label>
                <br/>
                <img src="<?php echo $this->main->image_preview_url($edit->thumbnail) ?>" class="img-thumbnail" width="200">
                <br/><br/>
                <div class="custom-file">
                    <input type="file" class="custom-file-input browse-preview-img" accept="image/*" name="thumbnail"
                           id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <span class="form-text text-muted"><?php echo $this->main->file_info() ?></span>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Thumbnail Alt</label>
                <input type="text" class="form-control" name="thumbnail_alt" value="<?php echo $edit->thumbnail_alt ?>">
                <span class="form-text text-muted"><?php echo $this->main->help_thumbnail_alt() ?></span>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Available</label>
                <input type="text" class="form-control" name="available" value="<?php echo $edit->available ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Duration</label>
                <input type="text" class="form-control" name="duration" value="<?php echo $edit->duration ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Pickup</label>
                <input type="text" class="form-control" name="pickup" value="<?php echo $edit->pickup ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Group Size</label>
                <input type="text" class="form-control" name="group_size" value="<?php echo $edit->group_size ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Minimum Age</label>
                <input type="text" class="form-control" name="minimum_age" value="<?php echo $edit->minimum_age ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Language</label>
                <input type="text" class="form-control" name="language" value="<?php echo $edit->language ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Start From</label>
                <input type="text" class="form-control" name="start_from" value="<?php echo $edit->start_from ?>">
            </div>
            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                <label>Description</label>
                <textarea class="tinymce" rows="3" name="description"><?php echo $edit->description ?></textarea>
            </div>
            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                <label>Include</label>
                <textarea class="tinymce" rows="3" name="include"><?php echo $edit->include ?></textarea>
            </div>
            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                <label>Exclude</label>
                <textarea class="tinymce" rows="3" name="exclude"><?php echo $edit->exclude ?></textarea>
            </div>
            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                <label>Optional</label>
                <textarea class="tinymce" rows="3" name="optional"><?php echo $edit->optional ?></textarea>
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Meta title</label>
                <input type="text" class="form-control" placeholder="Meta Title" name="meta_title" value="<?php echo $edit->meta_title ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Meta Description</label>
                <input type="text" class="form-control" placeholder="Meta description" name="meta_description" value="<?php echo $edit->meta_description ?>">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Meta Keywords</label>
                <input type="text" class="form-control" placeholder="Meta keywords" name="meta_keywords" value="<?php echo $edit->meta_keywords ?>">
            </div>
                <a href="<?php echo base_url('intiru/tour') ?>" class="btn btn-secondary">Back</a>
                <input type="submit" class="btn btn-primary" name="submit" value="Update">
        </form>
    </div>
</div>




