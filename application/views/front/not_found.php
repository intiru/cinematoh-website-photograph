<section class="attractions-desc">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="space" style="height: 100px"></div>
                <div class="section-title">
                    <h1>Not Found</h1>
                </div>
            </div>
            <div class="col-lg-12">
                <p align="center">Halaman Tidak Ditemukan, mohon untuk klik menu yang ada diatas</p>
            </div>
        </div>
    </div>
</section>`