<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <?php foreach ($blog_list as $row) {
                $permalink = $this->main->permalink(array('blog', $row->title)) ?>
                <div class="excerpt-item">
                    <div class="excerpt-img">
                        <a href="<?= $permalink ?>">
                            <img class="img-fluid" src="<?= $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>">
                        </a>
                        <div class="excerpt-date text-center">
                            <h3><?php echo date('d', strtotime($row->created_at)); ?></h3>
                            <span><?php echo date('F', strtotime($row->created_at)); ?></span>
                        </div>
                    </div>

                    <div class="excerpt-content">
                        <a href="<?= $permalink ?>">
                            <h3><?php echo $row->title ?></h3>
                        </a>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <?= $dict_kategori ?> : <a href="<?php echo $this->main->permalink(array('blog', $row->blog_category_title)) ?>"><?php echo $row->blog_category_title ?></a>
                            </li>
                            <li class="list-inline-item">
                                <?= $dict_membaca ?> : <a href="<?php echo $permalink ?>"><?php echo number_format($row->views) ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <?php echo $this->pagination->create_links() ?>

        </div>
        <div class="col-md-4">
            <div class="widget widget_recent_post">
                <h3 class="widget-title"><?= $dict_tulisan_terbaru ?></h3>
                <ul>
                    <?php foreach ($blog_recent as $row) { ?>
                        <li>
                            <a href="<?= $this->main->permalink(array('blog', $row->title)) ?>">
                                <img src="<?= $this->main->image_preview_url($row->thumbnail) ?>" style="width:100% !important" alt="<?php echo $row->thumbnail_alt ?>">
                                <div><?php echo date('d F Y', strtotime($row->created_at)) ?></div>
                                <p><strong><?php echo $row->title ?></strong></p>
                            </a>
                        </li>
                    <?php }  ?>
                </ul>
            </div>

            <div class="widget widget_categories">
                <h3 class="widget-title"><?= $dict_kategori_1 ?></h3>
                <ul>
                    <?php foreach ($blog_category as $row) { ?>
                        <li><a href="<?= $this->main->permalink(array('blog', $row->title)) ?>"><?php echo $row->title ?></a><span><?php echo number_format($row->blog_total) ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>