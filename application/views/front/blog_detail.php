<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?php echo $page->title ?></h1>
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="<?= site_url('') ?>"><?php echo $dict_beranda ?></a></li>
                        <li class="list-inline-item">/</li>
                        <li class="list-inline-item"><a href="<?= site_url('blog') ?>">Blog</a></li>
                        <li class="list-inline-item">/</li>
                        <li class="list-inline-item"><a href="<?= $this->main->permalink(array('blog', $page->title)) ?>"><?php echo $page->title ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="content-item">
                <div class="content-img">
                    <img class="img-fluid" src="<?= $this->main->image_preview_url($page->thumbnail) ?>" alt="<?php echo $page->thumbnail_alt ?>">
                    <div class="content-date text-center">
                        <h3><?php echo date('d', strtotime($page->created_at)) ?></h3>
                        <span><?php echo date('F', strtotime($page->created_at)) ?></span>
                    </div>
                </div>
                <div class="content-content">
                    <h3><?php echo $page->title ?></h3>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <?= $dict_kategori ?> : <a href="<?php echo $this->main->permalink(array('blog', $page->blog_category_title)) ?>"><?php echo $page->blog_category_title ?></a>
                        </li>
                        <li class="list-inline-item">
                            <?= $dict_membaca ?> : <a href="<?php echo $this->main->permalink(array('blog', $page->title)) ?>"><?php echo number_format($page->views) ?></a>
                        </li>
                    </ul>
                    <?php echo $page->description ?>
                </div>
            </div>
            <div class="social-share">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="<?= $facebook_link ?>"><i class="fa fa-facebook"></i>Facebook</a></li>
                    <li class="list-inline-item"><a href="single.html#"><i class="fa fa-twitter"></i>Twitter</a></li>
                    <li class="list-inline-item"><a href="single.html#"><i class="fa fa-google-plus"> Google</i></a></li>
                    <li class="list-inline-item"><a href="single.html#"><i class="fa fa-linkedin"></i>LinkedIn</a></li>
                </ul>
            </div>

        </div>
        <div class="col-md-4">
            <div class="widget widget_recent_post">
                <h3 class="widget-title"><?= $dict_tulisan_terbaru ?></h3>
                <ul>
                    <?php foreach ($blog_recent as $row) { ?>
                        <li>
                            <a href="<?= $this->main->permalink(array('blog', $row->title)) ?>">
                                <img src="<?= $this->main->image_preview_url($row->thumbnail) ?>" style="width:100% !important" alt="<?php echo $row->thumbnail_alt ?>">
                                <div><?php echo date('d F Y', strtotime($row->created_at)) ?></div>
                                <p><strong><?php echo $row->title ?></strong></p>
                            </a>
                        </li>
                    <?php }  ?>
                </ul>
            </div>
            <div class="widget widget_categories">
                <h3 class="widget-title"><?= $dict_kategori_1 ?></h3>
                <ul>
                    <?php foreach ($blog_category as $row) { ?>
                        <li><a href="<?= $this->main->permalink(array('blog', $row->title)) ?>"><?php echo $row->title ?></a><span><?php echo number_format($row->blog_total) ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>