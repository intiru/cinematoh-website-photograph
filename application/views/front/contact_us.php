<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>" alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-4 col-sm-6 wow fadeInUp link" data-wow-duration="1s">
            <a href="<?= $address_link ?>" target="_blank">
                <div class="service-item">
                    <img class="kontak-img" src="<?= base_url('assets/template_front/images/location-rumah-sunat-bali.png') ?>" alt="Lokasi Renon Rumah Sunat Bali">
                    <h2><?= $dict_lokasi_klinik ?> Renon</h2>
                    <p><?= $address ?></p>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-sm-6 wow fadeInUp link" data-wow-duration="1.5s">
            <a href="<?= $address_link_1 ?>" target="_blank">
                <div class="service-item">
                    <img class="kontak-img" src="<?= base_url('assets/template_front/images/location-rumah-sunat-bali.png') ?>" alt="Lokasi Dalung Rumah Sunat Bali">
                    <h2><?= $dict_lokasi_klinik ?> Dalung</h2>
                    <p><?= $address_1 ?></p>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-sm-6 wow fadeInUp link" data-wow-duration="1.5s">
            <a href="<?= $whatsapp_link ?>" target="_blank">
                <div class="service-item">
                    <img class="kontak-img" src="<?= base_url('assets/template_front/images/kontak-rumah-sunat-bali.png') ?>" alt="Kontak Rumah Sunat Bali">
                    <h2>Telp dan Email</h2>
                    <p><?= $telephone ?></p>
                    <p><a href="<?= $email_link ?>"><?= $email ?></a></p>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="space"></div>
<div id="contact" class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-12 wow fadeInLeft" data-wow-duration="2.5s">
            <div class="section-title">
                <span><?= $info_contact_us->title_sub ?></span>
                <h2><?= $info_contact_us->title ?></h2>
                <img src="<?= base_url('assets/template_front/images/dot-bluecolor.png') ?>" alt="Dekorasi Website Rumah Sunat Bali">
            </div>
            <div class="space" style="height: 50px"></div>
            <div class="row contact-form">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="<?= $dict_nama ?>">
                </div>
                <div class="col-md-6">
                    <input type="email" class="form-control" placeholder="Email">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Telephone">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Subject">
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" rows="3"></textarea>
                    <div class="thegncy-btn">
                        <input class="contact-btn" type="submit" value="<?= $dict_kirim_sekarang ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 d-sm-none d-xs-none d-lg-block wow fadeInRight" data-wow-duration="2.5s">
            <img class="img-fluid imgs" src="<?= base_url('assets/template_front/images/layanan/paketharga.png') ?>" alt="Kontak Rumah Sunat Bali">
        </div>
    </div>
    <div class="space"></div>
    <div class="map-iframe">
        <iframe src="<?= $maps ?>" width="100%" height="400" frameborder="0" allowfullscreen=""></iframe>
    </div>
</div>