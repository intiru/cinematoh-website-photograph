<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>" alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div class="container">
    <div class="panel pricing-table">

        <?php foreach ($category_price_list as $row) { ?>
            <div class="pricing-plan">
                <img src="<?= $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->title ?>" class="pricing-img">
                <h4 class="pricing-header"><?= $row->title ?></h4>
                <div class="pricing-features">
                    <ul>
                        <li><?= $row->description ?></li>
                    </ul>
                </div>
                <span class="pricing-price"><?= $row->price ?></span>
                <a href="<?php echo $whatsapp_link ?>" target="_blank"><img src="<?= base_url('assets/template_front/images/icons/pesan-paket-sunat-via-whatsapp.png') ?>" alt="Pesan Paket Sunat <?php echo $row->title ?> via WhatsApp" class="ukuran"></a>
            </div>
        <?php } ?>

    </div>
</div>