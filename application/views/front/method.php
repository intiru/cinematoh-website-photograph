<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>" alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div id="features" class="container">
    <div class="row">
        <div class="col-lg-5 wow fadeInLeft" data-wow-duration="2.5s">
            <div class="section-title">
                <span><?= $info_method->title_sub ?></span>
                <h1><?= $info_method->title ?></h1>
                <img src="<?= base_url('assets/template_front/images/dot-bluecolor.png') ?>" alt="<?php echo $info_method->title ?>">
            </div>
            <div class="space"></div>
            <?php

            foreach ($info_method_list['title'] as $key => $row) { ?>
                <div class="feature-item wow bounceInUp" data-wow-duration="2.5s">
                    <div class="feature-item-icon feature-block">
                        <img class="ic-post" alt="<?php echo $row ?>" src="<?= $this->main->image_preview_url($info_method_list['images'][$key]) ?>">
                    </div>
                    <div class="feature-item-content ">
                        <h5><?php echo $row ?></h5>
                        <p><?php echo $info_method_list['description'][$key] ?></p>
                    </div>
                </div>
            <?php }

            ?>

        </div>
        <div class="col-xl-5 col-lg-6 offset-xl-1 text-center wow fadeInRight" data-wow-duration="2.5s">
            <img class="img-fluid pos" data-tilt src="<?= $this->main->image_preview_url($info_method->thumbnail); ?>" alt="<?php echo $info_methd->thumbnail_alt ?>">

            <div class="pic-right-bottom">
                <img src="<?= base_url('assets/template_front/images/dot-squire.png') ?>" alt="Dekorasi Website Rumah Sunat Bali">
            </div>

        </div>
    </div>
</div>