<section id="footer" class="footer-minimal no-margin-top bg-transparent">
    <div class="footer-inner">
        <div class="footer-container tt-wrap">
            <div class="row">
                <div class="col-md-6 col-md-push-6">

                    <!-- Begin social buttons -->
                    <div class="social-buttons">
                        <ul>
                            <li><a href="https://www.facebook.com/themetorium" class="btn btn-social-min btn-default btn-link" title="Follow me on Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://twitter.com/Themetorium" class="btn btn-social-min btn-default btn-link" title="Follow me on Twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/+SiiliOnu" class="btn btn-social-min btn-default btn-link" title="Follow me on Google+" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="https://www.pinterest.com/themetorium" class="btn btn-social-min btn-default btn-link" title="Follow me on Pinterest" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="https://dribbble.com/Themetorium" class="btn btn-social-min btn-default btn-link" title="Follow me on Dribbble" target="_blank"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="contact.html" class="btn btn-social-min btn-default btn-link" title="Drop me a line" target="_blank"><i class="fas fa-envelope"></i></a></li>
                        </ul>
                    </div>
                    <!-- End social buttons -->

                </div> <!-- /.col -->

                <div class="col-md-6 col-md-pull-6">

                    <!-- Begin footer copyright -->
                    <div class="footer-copyright">
                        <p>&copy; Sepia 2017 / All rights reserved</p>
                        <p>Design by: <a href="https://www.mahendrawardana.com" target="_blank">Themetorium</a></p>
                    </div>
                    <!-- End footer copyright -->

                </div> <!-- /.col -->
            </div> <!-- /.row -->
        </div> <!-- /.footer-container -->
    </div> <!-- /.footer-inner -->
</section>