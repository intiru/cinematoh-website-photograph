<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$lang_code_list = array('id', 'en');


$route['default_controller'] = 'home';
$route['intiru'] = 'intiru/login';
$route['intiru/login'] = 'intiru/login/process';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$connection = mysqli_connect('localhost', 'root', '', 'intiru_cinematoh');


$blog = mysqli_query($connection, "SELECT id, title FROM blog");
$blog_category = mysqli_query($connection, "SELECT id, title FROM blog_category");
$menu = mysqli_query($connection, "SELECT title_menu, controller_method FROM pages WHERE type IN ('services', 'testimonial', 'method', 'price_list', 'blog', 'contact_us')");

while ($data = mysqli_fetch_array($menu)) {
    foreach ($lang_code_list as $code) {
        $route[$code . '/' . slug($data['title_menu'])] = $data['controller_method'];
        $route[$code . '/' . slug($data['title_menu']).'/(:num)'] = $data['controller_method'];
    }
}

while ($data = mysqli_fetch_array($blog)) {
    foreach ($lang_code_list as $code) {
        $route[$code . '/blog/' . slug($data['title'])] = "blog/detail/" . $data['id'];
    }
}
while ($data = mysqli_fetch_array($blog_category)) {
    foreach ($lang_code_list as $code) {
        $route[$code . '/blog/' . slug($data['title'])] = "blog/category/" . $data['id'];
        $route[$code . '/blog/' . slug($data['title']) . '/(:num)'] = "blog/category/" . $data['id'];
    }
}

foreach ($lang_code_list as $code) {

    $route['^' . $code . '/(.+)$'] = "$1";
    $route['^' . $code . '$'] = $route['default_controller'];
}

function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '+');
    $replace = array('-', '-', 'and', '-', '-', '-', '', '', '-plus');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
