<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tour_packages extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function list()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'tour_packages', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['category'] = $this->db->where('id_language', $data['id_language'])->get('category')->result();
        $data['footer_invite_tour'] = $this->db
            ->where('type', 'home_invite_tour')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $this->template->front('tour_packages', $data);
    }

    public function category($id_category = '')
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where('id', $id_category)->get('category')->row();
        $data['page']->type = 'tour_packages';
        $data['tour'] = $this->db->where('id_category', $id_category)->get('tour')->result();
        $data['footer_invite_tour'] = $this->db
            ->where('type', 'home_invite_tour')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $this->template->front('tour_packages_category', $data);
    }

    public function tour_detail($id_tour = '')
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->select('tour.*, category.title AS category_title')
            ->where('tour.id', $id_tour)
            ->join('category', 'category.id = tour.id_category', 'left')
            ->get('tour')
            ->row();
        $data['page']->type = 'tour_packages';

        $data['destination'] = $this->db->where_in('id', json_decode($data['page']->id_destination_json, TRUE))->get('destination')->result();

        $data['related'] = $this
            ->db
            ->select('tour.title,tour.thumbnail,tour.thumbnail_alt,duration,available,start_from, category.title AS category_title')
            ->join('category', 'category.id = tour.id_category')
            ->where_not_in('tour.id', array($id_tour))
            ->where('tour.id_language', $data['id_language'])
            ->order_by('tour.id', 'DESC')
            ->get('tour', 3, 0)
            ->result();


        $this->template->front('tour_detail', $data);
    }
}
