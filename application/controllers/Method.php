<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Method extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('method');

        $data['page'] = $this->db->where(array('type' => 'method', 'id_language' => $data['id_language']))->get('pages')->row();



        $data['info_method'] = $this
            ->db
            ->where(array(
                'type' => 'info_method',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['info_method_list'] = json_decode($data['info_method']->data_1, TRUE);

        $this->template->front('method', $data);
    }
}
