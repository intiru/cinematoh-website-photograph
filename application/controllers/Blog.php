<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Blog extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
    }

    public function index()
    {
        $offset = $this->uri->segment(3);
        $keyword = $_GET['search'];
        $limit = 5;

        $data = $this->main->data_front('blog');
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();


        if ($keyword) {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use' => 'yes'
                ))
                ->like('title', $keyword)
                ->like('description', $keyword)
                ->get('blog')
                ->num_rows();
        } else {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use' => 'yes'
                ))
                ->get('blog')
                ->num_rows();
        }


        $this->load->library('pagination');
        $config['base_url'] = site_url('blog');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $limit;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        if ($keyword) {
            $data['blog_list'] = $this
                ->db
                ->select('blog.*, blog_category.title AS blog_category_title')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
                ->where(array(
                    'blog.id_language' => $data['id_language'],
                    'blog.use' => 'yes'
                ))
                ->like('blog.title', $keyword)
                ->or_like('blog.description', $keyword)
                ->order_by('blog.id', 'DESC')
                ->get('blog', $limit, $offset)
                ->result();
        } else {
            $data['blog_list'] = $this
                ->db
                ->select('blog.*, blog_category.title AS blog_category_title')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
                ->where(array(
                    'blog.id_language' => $data['id_language'],
                    'blog.use' => 'yes'
                ))
                ->order_by('blog.id', 'DESC')
                ->get('blog', $limit, $offset)
                ->result();
        }

        $data['blog_recent'] = $this
            ->db
            ->where(array(
                'blog.use' => 'yes',
                'blog.id_language' => $data['id_language']
            ))
            ->order_by('id', 'DESC')
            ->limit(4)
            ->offset(0)
            ->get('blog')
            ->result();

        $data['blog_category'] = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('blog_category')
            ->result();

        foreach ($data['blog_category'] as $row) {
            $row->blog_total = $this
                ->db
                ->where(array(
                    'id_blog_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('blog')
                ->num_rows();
        }

        $data['keyword'] = $keyword;
        // $data['blog_sidebar'] = $this->blog_sidebar($data);


        $this->template->front('blog', $data);
    }

    public function category($id_blog_category = '')
    {
        $uri_2 = $this->uri->segment(2);
        $uri_3 = $this->uri->segment(3);
        $offset = $this->uri->segment(4);
        $limit = 5;

        $data = $this->main->data_front();
        $data['page'] = $this->db->where('id', $id_blog_category)->get('blog_category')->row();
        $data['page']->type = 'blog';

        $jumlah_data = $this->db->where(array('use' => 'yes', 'id_blog_category' => $id_blog_category))->get('blog')->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url($uri_2 . '/' . $uri_3);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $limit;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);


        $data['blog_list'] = $this
            ->db
            ->select('blog.*, blog_category.title AS blog_category_title')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where(array('blog.use' => 'yes', 'blog.id_blog_category' => $id_blog_category))
            ->order_by('blog.id', 'DESC')
            ->get('blog', $limit, $offset)
            ->result();
        $data['id_blog_category'] = $id_blog_category;

        $data['blog_recent'] = $this
            ->db
            ->where(array(
                'blog.use' => 'yes',
                'blog.id_language' => $data['id_language']
            ))
            ->order_by('id', 'DESC')
            ->limit(4)
            ->offset(0)
            ->get('blog')
            ->result();

        $data['blog_category'] = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('blog_category')
            ->result();

        foreach ($data['blog_category'] as $row) {
            $row->blog_total = $this
                ->db
                ->where(array(
                    'id_blog_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('blog')
                ->num_rows();
        }

        $this->template->front('blog_category', $data);
    }

    public function date($year, $month)
    {
        $uri_2 = $this->uri->segment(2);
        $uri_3 = $this->uri->segment(3);
        $uri_4 = $this->uri->segment(4);
        $offset = $this->uri->segment(5);

        $data = $this->main->data_front();
        $data['page'] = (object) array(
            'title' => 'Blog Tahun ' . $year . ' ' . ucwords($month),
            'type' => 'blog',
            'meta_title' => 'Blog Tahun ' . $year . ' ' . ucwords($month),
            'meta_keywords' => 'arsip blog ' . $year . ', arip asuransi ' . $year . ' ' . $month,
            'meta_description' => 'Blog Tahun ' . $year . ' ' . ucwords($month),
        );

        $month = date('n', strtotime($year . '-' . ucwords($month) . '-1'));

        $jumlah_data = $this
            ->db
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->where('MONTH(created_at)', $month)
            ->where('YEAR(created_at)', $year)
            ->get('blog')
            ->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url($uri_2 . '/' . $uri_3 . '/' . $uri_4);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 6;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);


        $data['blog_list'] = $this
            ->db
            ->where(array('blog.use' => 'yes'))
            ->order_by('blog.id', 'DESC')
            ->where('MONTH(created_at)', $month)
            ->where('YEAR(created_at)', $year)
            ->get('blog', 5, $offset)
            ->result();

        $this->template->front('blog_category', $data);
    }

    public function detail($id)
    {
        $data = $this->main->data_front();
        $views = $this->db->select('views')->where('id', $id)->get('blog')->row()->views;
        $views++;
        $this->db->where('id', $id)->update('blog', array('views' => $views));
        $data['views'] = $views;
        $data['page'] = $this
            ->db
            ->select('blog.*, blog_category.title AS blog_category_title')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.id', $id)
            ->order_by('blog.id', 'DESC')
            ->get('blog')
            ->row();

        $data['page']->type = 'blog';

        $data['blog_recent'] = $this
            ->db
            ->where(array(
                'blog.use' => 'yes',
                'blog.id_language' => $data['id_language']
            ))
            ->where_not_in('id', array($id))
            ->order_by('id', 'DESC')
            ->limit(4)
            ->offset(0)
            ->get('blog')
            ->result();

        $data['blog_category'] = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('blog_category')
            ->result();

        foreach ($data['blog_category'] as $row) {
            $row->blog_total = $this
                ->db
                ->where(array(
                    'id_blog_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('blog')
                ->num_rows();
        }

        $this->template->front('blog_detail', $data);
    }

    public function blog_sidebar($data)
    {

        $data['blog_popular'] = $this
            ->db
            ->select('title, thumbnail, thumbnail_alt, created_at')
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->where_not_in('id', array('id', $data['page']->id))
            ->order_by('views', 'DESC')
            ->get('blog', 4, 0)
            ->result();
        $blog_category = $this
            ->db
            ->select('title')
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('blog_category')
            ->result();

        $year_now = date('Y');
        $blog_archives = array();
        for ($m = 1; $m <= date('n'); $m++) {
            $month = date('F', mktime(0, 0, 0, $m, 1, $year_now));
            $blog_archives[$month] = $this
                ->db
                ->where('use', 'yes')
                ->where('MONTH(created_at)', $m)
                ->where('YEAR(created_at)', $year_now)
                ->get('blog')
                ->num_rows();
        }

        foreach ($blog_category as $row) {
            $row->total = $this
                ->db
                ->where(array(
                    'id_blog_category' => $row->id
                ))
                ->get('blog')
                ->num_rows();
        }

        $data['blog_category'] = $blog_category;
        $data['blog_archives'] = $blog_archives;
        $data['year_now'] = $year_now;

        return $this->load->view('front/blog_sidebar', $data, TRUE);
    }
}
