<?php
defined('BASEPATH') or exit('No direct script access allowed');

class testimonial extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front('testimonial');

		$data['page'] = $this->db->where(array('type' => 'testimonial', 'id_language' => $data['id_language']))->get('pages')->row();

		$data['testimonial_list'] = $this->db->where('use', 'yes')->order_by('id', 'ASC')->get('testimonial')->result();

		$this->template->front('testimonial', $data);
	}
}
