<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where(array('type' => 'about_us', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('about_us', $data);
    }

    public function team($id)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('team')
            ->row();
        $data['page']->type = 'profile';

        $this->template->front('team_detail', $data);
    }
}
