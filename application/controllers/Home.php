<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('home');

        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();
        // $data['slider'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->order_by('id', 'ASC')->get('slider')->result();
        // $data['home_tour'] = $this
        //     ->db
        //     ->select('tour.*, category.title AS category_title')
        //     ->join('category', 'category.id = tour.id_category')
        //     ->where('tour.id_language', $data['id_language'])
        //     ->order_by('title', 'ASC')
        //     ->get('tour', 6, 0)
        //     ->result();
        //        $data['testimonial_list'] = $this->db->where('use', 'yes')->order_by('id', 'DESC')->get('comment', 5, 0)->result();
        // $sesi_home = $this->db
        //     ->where_in('type', array(
        //         'home_banner',
        //         'info_services'
        //     ))
        //     ->where('id_language', $data['id_language'])
        //     ->get('pages')
        //     ->result();

        // foreach ($sesi_home as $row) {
        //     $data[$row->type] = $row;
        // }

        $data['home_banner'] = $this
            ->db
            ->where(array(
                'type' => 'home_banner',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['home_banner_list'] = json_decode($data['home_banner']->data_1, TRUE);

        $data['info_services'] = $this
            ->db
            ->where(array(
                'type' => 'info_services',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['info_services_list'] = json_decode($data['info_services']->data_1, TRUE);

        $data['info_method'] = $this
            ->db
            ->where(array(
                'type' => 'info_method',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['info_method_list'] = json_decode($data['info_method']->data_1, TRUE);

        $data['bar_contact'] = $this
            ->db
            ->where(array(
                'type' => 'bar_contact',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['info_blog'] = $this
            ->db
            ->where(array(
                'type' => 'info_blog',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['category_services'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->get('category_services')->result();

        $data['bar_consultation'] = $this
            ->db
            ->where(array(
                'type' => 'bar_consultation',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['blog_recent'] = $this
            ->db
            ->select('blog.*, blog_category.title AS blog_category_title')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where(array(
                'blog.use' => 'yes',
                'blog.id_language' => $data['id_language']
            ))
            ->order_by('id', 'DESC')
            ->limit(3)
            ->offset(0)
            ->get('blog')
            ->result();


        $json = file_get_contents('https://app.hubpasien.com/patient/done/total');
        $obj = json_decode($json, TRUE);
        $data['patient_done_total'] = $obj['data']['patient_done_total'];


        //        $data['blog_related'] = $this
        //            ->db
        ////            ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
        ////            ->join('team', 'team.id = blog.id_team', 'left')
        ////            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
        //            ->where('blog.use', 'yes')
        //            ->where('blog.id_language', $data['id_language'])
        //            ->order_by('blog.id', 'DESC')
        //            ->get('blog', 3, 0)
        //            ->result();

        //        echo json_encode($data);
        //        exit;

        $this->template->front('home_content', $data);
    }

    // function modal_announcement_hide()
    // {
    //     $this->session->set_userdata(array('modal_announcement_status' => 'hide'));
    // }
}
