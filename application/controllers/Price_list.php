<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Price_list extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front('price_list');
		$data['page'] = $this->db->where(array('type' => 'price_list', 'id_language' => $data['id_language']))->get('pages')->row();

		$data['category_price_list'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->get('category_price_list')->result();
		$this->template->front('price_list', $data);
	}
}
