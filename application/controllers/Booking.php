<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function list()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'booking', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['tour'] = $this
            ->db
            ->select('tour.*, category.title AS category_title')
            ->join('category', 'category.id = tour.id_category')
            ->where('tour.id_language', $data['id_language'])
            ->order_by('category.title', 'ASC')
            ->order_by('tour.title', 'ASC')
            ->get('tour')
            ->result();
        $this->template->front('booking', $data);
    }
}
