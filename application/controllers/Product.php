<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $page = $this
            ->db
            ->where(array('id_language' => $data['id_language']))
            ->where_in('type', array('about_us', 'home_sesi_6'))
            ->get('pages')
            ->result();
        foreach ($page as $row) {
            $data[$row->type] = $row;
        }

        $data['page'] = $data['about_us'];
        $this->template->front('tentang_kami', $data);
    }

    public function category($id = '')
    {

        $data = $this->main->data_front();
        $uri_2 = $this->uri->segment(2);
        $offset = $this->uri->segment(3);
        $jumlah_data = $this
            ->db
            ->where(array(
                'id_category' => $id,
                'id_language' => $data['id_language'],
                'data_type' => 'product_description'
            ))
            ->get('product')
            ->num_rows();

        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('category')
            ->row();

        $this->load->library('pagination');
        $config['base_url'] = site_url($uri_2);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 6;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);


        $data['product_list'] = $this
            ->db
            ->select('id, title, thumbnail, description, created_at')
            ->where(array(
                'id_category' => $id,
                'id_language' => $data['id_language'],
                'data_type' => 'product_description'
            ))
            ->order_by('title', 'ASC')
            ->get('product', 9, $offset)
            ->result();



        $this->template->front('category', $data);
    }

    public function detail($id = '')
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->select('product.*, category.title AS category_title')
            ->where('product.id', $id)
            ->join('category', 'category.id = product.id_category', 'left')
            ->get('product')
            ->row();

        $product_parent = $this->db->select('data_type, id, id_parent')->where('id', $data['page']->id_parent)->get('product')->row();

        if($product_parent->data_type == 'top_menu') {
            $id_product_top_menu = $product_parent->id;
        } elseif($product_parent->data_type == 'sub_menu_1') {
            $id_product_top_menu = $this->db->select('id')->where('id', $product_parent->id_parent)->get('product')->row()->id;
        }

        $data['id_product_top_menu'] = $id_product_top_menu;
        $data['related'] = $this
            ->db
            ->select('title,thumbnail,meta_title,created_at')
            ->where('id_category', $data['page']->id_category)
            ->order_by('id', 'DESC')
            ->get('product', 5, 0)
            ->result();
        foreach ($data['category_list'] as $row) {
            $row->total = $this
                ->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'id_category' => $row->id
                ))
                ->get('product')
                ->num_rows();
        }


        $this->template->front('product_detail', $data);
    }
}
